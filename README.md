# Rancher-Traefik-Samples

This project aims is to hosted yaml files wich may be used to deploy application inside of an Rancher server with **Traefik v2** as ingress-controller.

The examples is provide without any guarantee. You need to be fluent with Rancher/Kubernetes/Traefik.

Applications Lists:
  * calibre-web
  * gitea
  * funkwhale
  * jellyfin
